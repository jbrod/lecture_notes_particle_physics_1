#-
* form compton2.frm

v p,k,pp,kp;
v e,ep,es,eps;
i mu,nu;
s me;

l exp =   (g_(1,pp)+me*gi_(1))
        * ((2*g_(1,nu)*p(mu) + g_(1,nu,k,mu))/p.k - (2*g_(1,mu)*p(nu) - g_(1,mu,kp,nu))/p.kp)
        * (g_(1,p)+me*gi_(1)) 
        * ((2*g_(1,nu)*p(mu) + g_(1,mu,k,nu))/p.k - (2*g_(1,mu)*p(nu) - g_(1,nu,kp,mu))/p.kp) ;

trace4,1;

* on-shell conditions:
id k.k = 0;
id kp.kp = 0;

id p.p = me^2;
id pp.pp = me^2;

* momentum conservation:
repeat;
id k.pp = p.kp;
id pp.kp = p.k;
id pp.p = k.kp + me^2;

id k.kp = k.p - k.pp;
endrepeat;

b me,e,ep;
print +s;
.end