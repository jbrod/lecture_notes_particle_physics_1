#-
* form compton.frm

v p,k,pp,kp;
v e,ep,es,eps;
s me;

l exp =   (g_(1,pp)+me*gi_(1))
        * (g_(1,eps,k,e)/p.k + g_(1,e,kp,eps)/p.kp)
        * (g_(1,p)+me*gi_(1)) 
        * (g_(1,es,k,ep)/p.k + g_(1,ep,kp,es)/p.kp) ;

trace4,1;

* Linear polarization:
id es = e;
id eps = ep;

* on-shell conditions:
id k.k = 0;
id kp.kp = 0;

* polarization vector identities:
id k.e = 0;
id kp.ep = 0;

id e.e = 1;
id ep.ep = 1;

* Gauge condition:
id e.p = 0;
id p.ep = 0;

* momentum conservation:
repeat;
id k.pp = p.kp;
id pp.kp = p.k;
id pp.p = k.kp + me^2;
id pp.ep = k.ep;
id pp.e = -kp.e;

id k.kp = k.p - k.pp;
endrepeat;

b me,e,ep;
print +s;
.end